# Maintainer: Minecrell <minecrell@minecrell.net>
# Kernel config based on: arch/arm64/configs/msm8916_defconfig

_flavor="postmarketos-qcom-msm8916"
pkgname=linux-$_flavor
pkgver=5.11.7
pkgrel=0
pkgdesc="Mainline kernel fork for Qualcomm MSM8916 devices"
arch="aarch64 armv7"
url="https://github.com/msm8916-mainline/linux"
license="GPL-2.0-only"
options="!strip !check !tracedeps pmb:cross-native pmb:kconfigcheck-anbox"
makedepends="bison findutils flex installkernel openssl-dev perl gmp-dev mpc1-dev mpfr-dev"

# Architecture
case "$CARCH" in
	aarch64) _carch="arm64" ;;
	arm*)    _carch="arm" ;;
esac

# Source
_tag=v${pkgver//_/-}-msm8916
source="
	$pkgname-$_tag.tar.gz::$url/archive/$_tag.tar.gz
	config-$_flavor.aarch64
	config-$_flavor.armv7
"
builddir="$srcdir/linux-${_tag#v}"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$CARCH" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION=$((pkgrel + 1 ))
}

package() {
	mkdir -p "$pkgdir"/boot
	make zinstall modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_PATH="$pkgdir"/boot \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_DTBS_PATH="$pkgdir"/usr/share/dtb
	rm -f "$pkgdir"/lib/modules/*/build "$pkgdir"/lib/modules/*/source

	install -D "$builddir"/include/config/kernel.release \
		"$pkgdir"/usr/share/kernel/$_flavor/kernel.release
}

sha512sums="169d88a6fb61dd63d89330d6c5f76a208b49dbc777b7fa426da888c7d3f45356a4765a2096b3894b8c2606d97e2ca209f3ef244540094877c48533343999eb5a  linux-postmarketos-qcom-msm8916-v5.11.7-msm8916.tar.gz
548bedd13d28a22e9a3049de6a906567a51e8ae76385858d5cfc599dfbbf5ca3371a230bcbaa31780f8dd53682e07ba8e7ab48788d24568003356172f209f8bc  config-postmarketos-qcom-msm8916.aarch64
e579e8c8c8640219303efa270273fbb28caf7b45e51098a49bf0601cf5cff9b3f244c0cccefdd86cb93a2b39e796e572f2a28be9941d50b79f02a936118cf7ae  config-postmarketos-qcom-msm8916.armv7"
